# L1MuEReleaseControl
Welcome to the ReleaseControl package.
## Release Numbering Scheme
The online packages follow the numberling schema.  
A.B.X.Y  

| Digit | Meaning |
---- | ---- 
| A | Release series |
| B | Release flavour |
| X | Major Release Number |
| Y | Minor Release Number |  

Y is now often not used, in which case the release name is usually A.B.X.
Ref : https://twiki.cern.ch/twiki/bin/view/AtlasComputing/OfflineReleaseNumbering

### The B digit
| B | Purpose |
----|---- 
| 0 | Development at testbench |
| 1 | Standalone test at Point 1 | 
| 2 | Run-3 data taking |

## Links
https://twiki.cern.ch/twiki/bin/viewauth/Atlas/NewSectorLogicPhase1
